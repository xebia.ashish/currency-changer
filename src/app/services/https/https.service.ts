import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class HttpsService {
  baseUrl: string = environment.baseUrl;
  key: string = "&api_key=9f63b69861-18930291a5-r8djm7";
  public content = new BehaviorSubject<any>(false);    
  public share = this.content.asObservable(); 
  public homeContent = new BehaviorSubject<any>(null);    
  public fromHomeShare = this.homeContent.asObservable(); 
  public amount = new BehaviorSubject<any>(null);    
  public amountShare = this.amount.asObservable(); 

  constructor(private http: HttpClient) {}

  getCurrencies(): Observable<Object> {
    return this.http.get(this.baseUrl+"currencies?"+this.key);
  }

  convert(from:string,to:string): Observable<Object> {
    return this.http.get(this.baseUrl+"fetch-one?from="+from+"&to="+to+this.key);
  }

  fetchMulti(from:string): Observable<Object> {
    return this.http.get(this.baseUrl+"fetch-multi?from="+from+"&to=EUR,GBP,CHF"+this.key);
  }

  historicalData(from:string,date:string): Observable<Object> {
    return this.http.get(this.baseUrl+"historical?date="+date+"&from="+from+this.key);
  }

  header() {
    // if (localStorage.getItem('accessToken') != undefined || localStorage.getItem('accessToken') != null) {
      const headers = new HttpHeaders({
        "cache-control": "no-cache",
        "content-type": "application/json",
        authorization: localStorage.getItem('accessToken') || '',
      });
      const option = {
        headers,
      };
      return option;
    // }
  }
}
