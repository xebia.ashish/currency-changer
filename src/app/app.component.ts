import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpsService } from './services/https/https.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  amount: number = 0;
  to: string = "";

  constructor(private http: HttpsService, private route: ActivatedRoute) {
    this.http.amountShare.subscribe((res:any) => {
      this.amount = res;
    });
    this.http.fromHomeShare.subscribe((res:any) => {
      if(res&&res.from&&res.to) {
        this.to = res.to;
        this.amount && (this.amount = res.amount);
      }
    });
    this.route.params.subscribe((res: any) => {
      if(res&&res.from&&res.to) {
        this.to = res.to;
        this.amount && (this.amount = res.amount);
      }
    })
  }
  title = 'currency-exchanger';

  sendData(type:number) {
    let data:any;
    if(type===0) {
      data = {
        code: "EUR",
        name: "Euro"
      }
    }
    if(type===1) {
      data = {
        code: "USD",
        name: "United Sates Dollar"
      }
    }
    this.http.homeContent.next(data);
  }
}
