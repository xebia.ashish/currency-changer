import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpsService } from 'src/app/services/https/https.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnDestroy {
  currencies: any[] = [];
  details: any;
  rates: any[] = [];
  lastDayRates: any[] = [];
  secondLastDayRates: any[] = [];
  thirdLastDayRates: any[] = [];

  constructor(private http: HttpsService, private route: ActivatedRoute) {
    this.http.content.next(true);
    this.route.params.subscribe((res: any) => {
      if(res && res.from && res.to) {
        this.details = res;
        this.http.homeContent.next(this.details);
        this.getHistoricalData();
        this.fetchMulti();
      }
    });
  }

  getCurrencies() {
    this.http.getCurrencies().subscribe((res: any) => {
      this.currencies = Object.entries(res.currencies);
    });
  }

  ngOnDestroy(): void {
    this.http.content.next(false);
  }

  fetchMulti() {
    this.http.fetchMulti(this.getCurrencyCode(0)).subscribe((res: any) => {
      if (res) {
        this.rates = Object.entries(res.results);
      }
    })
  }

  getCurrencyCode(type:number):any {
    if(type===0) {
      return this.details.from.split(',')[0];
    }
    if(type===1) {
      return this.details.to.split(',')[0];
    }
  }
  
  getCurrencyName(type:number):any {
    if(type===0) {
      return this.details.from.split(',')[1];
    }
    if(type===1) {
      return this.details.to.split(',')[1];
    }
  }

  getHistoricalData() {
    // Last day
    const lastDayDate = new Date();
    lastDayDate.setDate(lastDayDate.getDate() - 1);
    this.http.historicalData(this.getCurrencyCode(0), this.formatDate(lastDayDate)).subscribe((res: any) => {
      this.lastDayRates = Object.entries(res.results);
    });
    // 2nd Last day
    const secondLastDate = new Date();
    secondLastDate.setDate(secondLastDate.getDate() - 2);
    this.http.historicalData(this.getCurrencyCode(0), this.formatDate(secondLastDate)).subscribe((res: any) => {
      this.secondLastDayRates = Object.entries(res.results);
    });
    // 3rd Last day
    const thirdLastDate = new Date();
    thirdLastDate.setDate(thirdLastDate.getDate() - 3);
    this.http.historicalData(this.getCurrencyCode(0), this.formatDate(thirdLastDate)).subscribe((res: any) => {
      this.thirdLastDayRates = Object.entries(res.results);
    });
  }

  formatDate(date: Date): any {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

}
