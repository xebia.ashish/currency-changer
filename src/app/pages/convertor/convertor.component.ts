import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpsService } from 'src/app/services/https/https.service';

@Component({
  selector: 'app-convertor',
  templateUrl: './convertor.component.html',
  styleUrls: ['./convertor.component.scss']
})
export class ConvertorComponent implements OnInit {
  from: string = "";
  to: string = "";
  amount: number = 0;
  isDisabled: boolean = true;
  isSwapDisabled: boolean = true;
  showResult: boolean = false;
  constantList: number[] = [];
  currencies: any[] = [];
  rate: number = 0;
  toRate: number = 0;
  fromDisabled: boolean = false;

  constructor(private http: HttpsService, private route: ActivatedRoute) {
    this.constantList = [10,25,55,110,225,500,1100];
    this.route.params.subscribe((res: any) => {
      if(res&&res.from&&res.to) {
        this.http.homeContent.next(res);
        this.from = res.from;
        this.to = res.to;
        this.amount = res.amount;
        this.checkDisabled();
      }
    })
    this.http.share.subscribe((res:any) => {
      this.fromDisabled = res;
    });
    this.http.fromHomeShare.subscribe((res:any) => {
      if(res&&res.from&&res.to) {
        this.from = res.from;
        this.to = res.to;
        this.amount = res.amount;
        this.checkDisabled();
      }
    });
  }

  ngOnInit(): void {
    this.getCurrencies();
  }

  getCurrencies() {
    this.http.getCurrencies().subscribe((res:any) => {
      this.currencies = Object.entries(res.currencies);
    })
  }

  convert() {
    this.showResult = true;
    this.http.convert(this.getCurrencyCode(0),this.getCurrencyCode(1)).subscribe((res:any) => {
      this.rate = res.result[this.getCurrencyCode(1)];
    })
    this.http.convert(this.getCurrencyCode(1),this.getCurrencyCode(0)).subscribe((res:any) => {
      this.toRate = res.result[this.getCurrencyCode(0)];
    })
  }
  
  swap() {
    if(this.from&&this.to) {
      let temp: string;
      temp = this.to;
      this.to = this.from;
      this.from = temp;
    }
  }
  
  getCurrencyCode(type:number):any {
    if(type===0) {
      return this.from.split(',')[0];
    }
    if(type===1) {
      return this.to.split(',')[0];
    }
  }
  
  getCurrencyName(type:number):any {
    if(type===0) {
      return this.from.split(',')[1];
    }
    if(type===1) {
      return this.to.split(',')[1];
    }
  }
  
  checkDisabled() {
    this.showResult = false;
    if(this.from&&this.to) {
      this.isSwapDisabled = false;
      if(this.amount&&this.amount>0) {
        this.http.amount.next(this.amount);
        this.isDisabled = false;
      } else {
        this.isDisabled = true;
      }
    } else {
      this.isDisabled = true;
      this.isSwapDisabled = true;
    }
  }
  
}
